import React, { useState } from "react";
import {
  Container,
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Collapse,
  NavbarToggler
} from "reactstrap";
import { NavLink as RRNavLink, withRouter } from "react-router-dom";
import "./header.scss";

const Header = (props) => {
    const [isOpen, setIsOpen ] = useState(false);
    return (
  <div className="header">
    <div className="contact-bar d-flex align-items-center">
      <Container className="d-flex align-items-center justify-content-between">
        <div className="contact-bar-left">
          <div className="email d-inline">support@despatchcloud.com</div>
          <div className="phone d-inline">01377 455 180</div>
        </div>
        <div className="contact-bar-right">Despatch Cloud</div>
      </Container>
    </div>
    <Container>
      <Navbar expand="md" className="px-0 text-white" light>
        <NavbarBrand>
            <span className="brand-text">
            {props.history.location.pathname=== "/"? "VAT Checker" :"Contact"}
            </span>
            </NavbarBrand>
        <NavbarToggler onClick={()=>setIsOpen(!isOpen)} className="text-white"/>
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink tag={RRNavLink} to="/" className="text-white nav-link-right" activeStyle={{fontWeight:700}} exact>
                Home
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={RRNavLink} to="/contact" className="text-white nav-link-right" activeStyle={{fontWeight:700}} exact>
                Contact
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </Container>
  </div>
)};

export default withRouter( Header);
