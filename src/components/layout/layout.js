import React from 'react';
import './layout.scss';
import Header from '../header/header';
const Layout = ({children}) => {
    return (
        <div className="layout">
            <Header />
            {children}
        </div>
    )
}

export default Layout;