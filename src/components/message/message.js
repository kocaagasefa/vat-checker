import React from "react";
import "./message.scss";

const Message = ({ data: { valid, title, trader_name, trader_address } }) => {
  return (
    <div className={["message", valid ? "valid" : "invalid"].join(" ")}>
      <div className="w-100">
        <div className="text-center w-100">{title}</div>
        {trader_name && (
          <div className="mt-4">
            <b>Trader Name: </b>
            {trader_name}
          </div>
        )}
        {trader_address && (
          <div>
            <b>Trader Address: </b>
            {trader_address}
          </div>
        )}
      </div>
    </div>
  );
};

export default Message;
