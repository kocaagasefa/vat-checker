import React from "react";
import "./vatChecker.scss";
import CheckForm from "../checkForm/checkForm";

const VatChecker = () => {
  return <div className="vat-checker">
      <h1 className="title">Vat Checker</h1>
      <CheckForm />
  </div>;
};

export default VatChecker;
