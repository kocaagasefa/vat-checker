import React, { useState, useCallback } from "react";
import "./checkForm.scss";
import Message from "../message/message";
import axios from "axios";
import { api_url } from "../../constants";
const CheckForm = () => {
  const [vat_number, setVatNumber] = useState("");
  const [message, setMessage] = useState(null);

  const check = useCallback(() => {
    axios
      .get(api_url, { params: { vat_number } })
      .then(res => {
        setMessage({
          title: res.data.data.valid
            ? "The number is valid on the specified date."
            : "The number is invalid on the specified date.",
          ...res.data.data
        });
      })
      .catch(error => {
        setMessage({
          valid: false,
          title: error.response.data.message
        });
      });
  }, [vat_number]);
  return (
    <>
      <div className="check-form">
        <div className="input-container">
          <input
            onChange={e => setVatNumber(e.target.value)}
            value={vat_number}
          />
        </div>
        <div className="submit-button" onClick={check}>
          <span className="submit-button-text">Check</span>
        </div>
      </div>
      {message && <Message  data={message}/>}
    </>
  );
};

export default CheckForm;
