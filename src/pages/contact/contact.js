import React from "react";
import { Container } from "reactstrap";
import "./contact.scss";

const Contact = () => (
  <Container className="contact">
    <p>
      Ut ullamco aliqua eu consequat ex nisi laborum. Quis Lorem ea magna labore
      proident proident do. Adipisicing esse occaecat est aute veniam et
      cupidatat ut ex consectetur. Ad tempor ex magna ea. Esse deserunt nulla
      nisi commodo ut ea culpa ut enim labore quis sit reprehenderit. Eiusmod
      incididunt occaecat labore et quis voluptate pariatur consequat. Eiusmod
      veniam fugiat sunt adipisicing cillum tempor nulla qui consectetur
      cupidatat.
    </p>
    <p>
      Do ipsum dolore minim aute dolore adipisicing reprehenderit Lorem fugiat
      aliqua dolor anim enim. Id aliqua enim dolore duis ea incididunt labore
      exercitation qui. Irure deserunt excepteur sint mollit veniam fugiat
      consequat cillum ipsum.
    </p>
    <p>
      Mollit occaecat exercitation do duis aliqua reprehenderit anim culpa dolor
      laborum enim ea. Occaecat exercitation dolore nulla enim quis ullamco
      mollit deserunt enim ullamco ut ullamco. Ipsum reprehenderit labore
      consequat exercitation do. Dolore ad aliqua reprehenderit exercitation
      laboris.
    </p>
    <p>
      Do tempor aliqua eu ut consequat et ullamco elit do ipsum labore aliquip
      do eiusmod. Ullamco magna eu irure quis proident cupidatat enim non labore
      cupidatat consequat voluptate adipisicing eiusmod. Ex ut labore nisi duis
      ex id ad id anim nisi laborum. Deserunt commodo ut sit dolor velit esse
      deserunt excepteur occaecat ea aliqua. Qui mollit aliqua reprehenderit
      exercitation sint occaecat velit voluptate quis culpa irure.
    </p>
    <p>
      In veniam ipsum duis exercitation do aute enim ullamco. Aliqua deserunt
      qui fugiat sunt aute esse est est dolore. Non nostrud minim ea tempor
      dolore consequat adipisicing et mollit.
    </p>
  </Container>
);

export default Contact;
