import React from "react";
import { Container, Row, Col } from "reactstrap";
import "./home.scss";
import VatChecker from "../../components/vatChecker/vatChecker";

const Home = () => {
  return (
    <div className="home">
      <Container>
        <Row className="d-flex align-items-end">
          <Col md={5}>
            <h1 className="home-title">Check the Value Added Tax Number</h1>
            <p className="home-desc">
              VAT Checker allows you to check the validity of a VAT number prior
              to applying the 0% rate when selling goods or services to EU
              countries.
            </p>
          </Col>
          <Col md={7}>
            <img
              src="/assets/images/building.png"
              alt="building"
              width="100%"
            />
          </Col>
        </Row>
      </Container>
      <VatChecker />
    </div>
  );
};

export default Home;
