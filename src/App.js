import React from "react";
import { Switch, BrowserRouter, Route } from "react-router-dom";
import "./App.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import Layout from "./components/layout/layout";
import Home from "./pages/home/home";
import Contact from "./pages/contact/contact";

function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route path="/contact" component={Contact} />
          <Route path="/" component={Home} />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
